// [SECTION] DEPENDENCIES
const Order = require("../models/Order");

// [SECTION] RETRIEVE ALL ORDERS (Admin only)
module.exports.getAllOrders = (req, res) => {

	Order.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err));
};
